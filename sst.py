#!/bin/python2
import csv, random, argparse

class SNLIClassifier:
    def __init__(self):
        train_file = open('data/train.csv', 'r')
        self.train_reader = csv.reader(train_file)

        valid_file = open('data/valid.csv', 'r')
        self.valid_reader = csv.reader(valid_file)

        test_in_file = open('data/test.csv', 'r')
        self.test_reader = csv.reader(test_in_file)

        # No header row to skip

    def train(self, subsample_ratio, filters):
        ## Your training logic goes here
        pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--subsample_ratio', nargs='?', const=1.0, type=int)
    parser.add_argument('--filters', nargs='?', const=100, type=int)

    args = parser.parse_args()
    trainer = SNLIClassifier()

    print('Training...')
    trainer.train(args.subsample_ratio, args.filters)

