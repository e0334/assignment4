# E0 334 Assignment 4

The deadline for this assignment is **Monday, October 29, 2018**.

In this assignment, you are tasked with implementing a classification pipeline using Yoon Kim's CNN with 3, 4 and 5 filters respectively for the [Stanford Sentiment Treebank](https://nlp.stanford.edu/sentiment/treebank.html) (SST-1). Please perform evaluation as described in class and plot your **training loss** and **test accuracy** after convergence against:

1. The number of filters used.
2. The size of the subsampled dataset.

## Getting Started

1. Please start by forking this repository (tick the checkbox for `This is a private repository`). This should result in the creation of a new repository under your account.
2. Edit the Python file to implement your solution. In case you are using Python 3, please update the Shebang (`#!`)  to reflect this.
3. Follow the submission instructions to share your solution with us. **Do not submit a Pull Request, unless you want to suggest a fix.**

## Submission Instructions

Please include both a `readme.md` required to reproduce your experiments and a `report.pdf` that contains your findings.

To submit your solution, go to `Settings > User and group access` and add `peteykun` with `Read` access to the `Users` section. We will automatically retrieve the latest submission via git.

